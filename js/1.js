$(function () {
  var st = $('<div class="ajax-loading__status"><div></div></div>').css({visibility : 'hidden'}).appendTo('body'),
    sl = st.find('div'),
	stt = $('<div class="ajax-loading__msg"></div>').css({visibility: 'hidden'}).appendTo('body'),
    last = 0,
    lock = $('<div class="ajax-loading__overlay"></div>').css({opacity : 0.6 , display : 'none'}).appendTo('body')
    .mousedown(function (E){
      E.preventDefault();
    });
  function lockScreen() {
    lock.fadeIn(300 , function () {
      lock.addClass('active');
    });
  }
  function unlockScreen() {
    lock.fadeOut(300 , function() {
      lock.removeClass('active');
    });
  }
  function status(t , p) {
    var getCompleted=0,animationStopped=0,g_d='';
	function makeStuff() {
		if (!getCompleted||!animationStopped) return;
		animationStopped = getCompleted = 0;
	    var t = $('#system-modules'),
                p = t.parent(),
                pr = t.prev(),
                ne = t.next();
            t.find('#edit-ajax-submit').unbind('click');
            t.remove();
            if (pr.length > 0) {
              pr.after(g_d);
            } else if (ne.length > 0) {
              ne.before(g_d);
            } else if (p.length > 0) {
              p.append(g_d);
            } else {
              return;
            }
            $('#system-modules #edit-ajax-submit').click(cl);
            unlockScreen();
	}
    last = p;
    if (p >= 100) {
	  stt.html(t);
      sl.stop().animate({width : '100%'},function () {
	    stt.fadeOut(1500);
        st.fadeOut(1500 , function () {
		  st.css({visibility : 'hidden' , display : 'block', top: '-1000px'});
		  stt.css({visibility : 'hidden' , display : 'block', top: '-1000px'});          
		  animationStopped = 1;
		  makeStuff();
        });
		$.get(Drupal.settings.basePath + 'ajax/minstall' , function(d) {
            getCompleted = 1;
			g_d=d;
			makeStuff();
          });
      });
      p = 0;
    } else
    {
      if (p == 0) {
	    sl.stop().css({
		  width: 0
		});
        st.css({
          display : 'none',
          visibility : 'visible',
          top : $(window).height()-st.height()
        }).fadeIn(300);
		stt.css({
		  display: 'none',
		  visibility: 'visible',
		  top: $(window).height()-st.height()
		}).fadeIn(300);
	  }
        sl.stop().animate({width : p + '%'});
		stt.html(t);
    }
  }
  function Iterator(act , p , c , sid) {
    $.post(Drupal.settings.basePath + 'ajax/install' , p || { sid : sid } , function (a) {
      var A = a.split('::'),
          S = sid ? sid : A[2];
      status(A[0] , A[1]);
      (A[1] - 0 < 100) && Iterator(act + 1 , null , c , S);
    });
  }
  var queue = [],
      block = 0,
      box = $('#system-modules #edit-ajax-submit').click(cl);
  function cl(E) {
    lockScreen();
    E.preventDefault();
    var p = {act : 'modules'};
    $('#system-modules input[name^=status].form-checkbox').each(function() {
      var _ = $(this);
      p[_.val()] = _.attr('checked');
    });
    Iterator(0 , p);
  }
});